package com.example.testappforvuz

import android.app.Application
import com.example.testappforvuz.data.AppContainer
import com.example.testappforvuz.data.AppDataContainer

class VuzApplication: Application() {
    lateinit var container: AppContainer
    override fun onCreate() {
        super.onCreate()
        container = AppDataContainer(this)
    }
}