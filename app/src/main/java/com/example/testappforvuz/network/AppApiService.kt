package com.example.testappforvuz.network

import com.example.testappforvuz.data.diode.Diode
import retrofit2.Response
import retrofit2.http.GET

interface AppApiService {
    @GET("diodes")
    suspend fun getDiodes(): Response<List<Diode>>
}