package com.example.testappforvuz.network

data class Order (
    val orderNumber: String,
    val listOrderItems: List<OrderItem>
)

data class OrderItem(val id: Int)