package com.example.testappforvuz.data

import com.example.testappforvuz.data.diode.Diode
import com.example.testappforvuz.network.AppApiService
import retrofit2.Response

interface AppRemoteRepository {
    suspend fun getDiodes(): Response<List<Diode>>
}

class DefaultAppRemoteRepository(
    private val appApiService: AppApiService
): AppRemoteRepository {
    override suspend fun getDiodes(): Response<List<Diode>> =
        appApiService.getDiodes()
}