package com.example.testappforvuz.data.diode

import kotlinx.coroutines.flow.Flow


interface DiodeRepository {

    fun getAllDiodes(): Flow<List<Diode>>

    fun getDiode(id: Int): Flow<Diode>

    suspend fun insertDiode(diode: Diode)

    suspend fun deleteDiode(diode: Diode)

    suspend fun updateDiode(diode: Diode)

}

class OfflineDiodeRepository(private val diodeDao: DiodeDao): DiodeRepository {

    override fun getAllDiodes(): Flow<List<Diode>> = diodeDao.getAllDiodes()

    override fun getDiode(id: Int): Flow<Diode> = diodeDao.getDiode(id)

    override suspend fun insertDiode(diode: Diode) = diodeDao.insert(diode)

    override suspend fun deleteDiode(diode: Diode) = diodeDao.delete(diode)

    override suspend fun updateDiode(diode: Diode) = diodeDao.update(diode)

}