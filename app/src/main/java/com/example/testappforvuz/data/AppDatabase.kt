package com.example.testappforvuz.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.testappforvuz.data.diode.Diode
import com.example.testappforvuz.data.diode.DiodeDao

@Database(entities = [Diode::class], version = 1, exportSchema = false)
abstract class AppDatabase: RoomDatabase(){

    abstract fun diodeDao(): DiodeDao

    companion object {
        @Volatile
        private var instance: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase {
            return instance ?: synchronized(this) {
                Room.databaseBuilder(context, AppDatabase::class.java, "app_database")
                    .fallbackToDestructiveMigration()
                    .build()
                    .also { instance = it }
            }
        }
    }

}