package com.example.testappforvuz.data.category


data class Category(
    val id: Int = 0,
    val title: Int,
    val path: String
)
