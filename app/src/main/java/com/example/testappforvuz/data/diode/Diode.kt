package com.example.testappforvuz.data.diode

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "diodes")
data class Diode (
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    val name: String,
    val vrwm: Int,
    val tol: Int,
    val housing: String,
)