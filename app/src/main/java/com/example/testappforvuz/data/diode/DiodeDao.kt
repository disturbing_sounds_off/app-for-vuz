package com.example.testappforvuz.data.diode

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import com.example.testappforvuz.data.diode.Diode
import kotlinx.coroutines.flow.Flow

@Dao
interface DiodeDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(diode: Diode)

    @Update
    suspend fun update(diode: Diode)

    @Delete
    suspend fun delete(diode: Diode)

    @Query("select * from diodes where id = :id")
    fun getDiode(id: Int): Flow<Diode>

    @Query("select * from diodes")
    fun getAllDiodes(): Flow<List<Diode>>

}