package com.example.testappforvuz.data

import android.content.Context
import com.example.testappforvuz.data.diode.DiodeRepository
import com.example.testappforvuz.data.diode.OfflineDiodeRepository
import com.example.testappforvuz.network.AppApiService
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

interface AppContainer {
    val remoteRepository: AppRemoteRepository
    val diodeRepository: DiodeRepository
}

class AppDataContainer(private val context: Context): AppContainer {

    // REMOTE REPOSITORY

    private val BASE_URL = "https://www.google.com/"

    private val retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    private val retrofitService: AppApiService by lazy {
        retrofit.create(AppApiService::class.java)
    }

    override val remoteRepository: AppRemoteRepository by lazy {
        DefaultAppRemoteRepository(retrofitService)
    }

    // LOCAL REPOSITORIES
    override val diodeRepository: DiodeRepository by lazy {
        OfflineDiodeRepository(AppDatabase.getDatabase(context).diodeDao())
    }

}