package com.example.inventory.ui.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Shapes
import androidx.compose.material.Typography
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import com.example.learnroom.ui.theme.Grey
import com.example.learnroom.ui.theme.Orange
import com.example.learnroom.ui.theme.Purple80
import com.example.learnroom.ui.theme.PurpleGrey40
import com.example.learnroom.ui.theme.Typography

private val DarkColorPalette = darkColors(
    primary = Orange,
    secondary = PurpleGrey40,
    onPrimary = Color.White
)

private val LightColorPalette = lightColors(
    primary = Orange,
    secondary = PurpleGrey40,
    onPrimary = Color.White,
    background = Grey
)

@Composable
fun LearnRoomTheme(darkTheme: Boolean = isSystemInDarkTheme(), content: @Composable () -> Unit) {
    val colors = if (darkTheme) {
        DarkColorPalette
    } else {
        LightColorPalette
    }

    MaterialTheme(
        colors = colors,
        typography = Typography,
        shapes = Shapes,
        content = content
    )
}
