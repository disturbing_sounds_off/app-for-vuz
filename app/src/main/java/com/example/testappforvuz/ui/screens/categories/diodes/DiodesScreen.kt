package com.example.testappforvuz.ui.screens.categories.diodes

import android.widget.Space
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Button
import androidx.compose.material.Card
import androidx.compose.material.Divider
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.inventory.ui.theme.LearnRoomTheme
import com.example.learnroom.ui.navigation.Screen
import com.example.testappforvuz.R
import com.example.testappforvuz.data.diode.Diode
import com.example.testappforvuz.ui.MyBottomBar


// TODO кривая тень у первого диода

@Composable
fun DiodesScreen(
    modifier: Modifier = Modifier,
    uiState: DiodeUiState,
    bottomNavigate: (String) -> Unit,
    canNavigateBack: Boolean,
    navigateBack: () -> Unit
) {

    var showFilters by remember{ mutableStateOf(false) }
    var tolFilter by remember {
        mutableStateOf("")
    }

    if (!showFilters) {
        Diodes(
            showFiltersOnPress = { showFilters = true },
            uiState = uiState,
            bottomNavigate = bottomNavigate,
            canNavigateBack = canNavigateBack,
            navigateBack = navigateBack
        )
    } else {
        DiodeFilters(
            tolFilter = tolFilter,
            tolOnValueChange = {tolFilter = it},
            onBackButtonPressed = {showFilters = false},
            bottomNavigate = bottomNavigate,
            applyFiltersAndGoBack = {
//                TODO applyFilters()
                showFilters = false
            }
        )
    }

}



@Composable
fun Diodes(
    showFiltersOnPress: () -> Unit,
    uiState: DiodeUiState,
    canNavigateBack: Boolean,
    navigateBack: () -> Unit,
    bottomNavigate: (String) -> Unit
) {

    Scaffold(
        topBar = {
            DiodeTopBar(
                title = stringResource(id = Screen.Diodes.title),
                canNavigateBack = canNavigateBack,
                navigateBack = {navigateBack()},
                showFilters = showFiltersOnPress
            )
        },

        bottomBar = {
            MyBottomBar(
                navigateToNewScreen = {newScreenRoute -> bottomNavigate(newScreenRoute) },
                currentRoute = Screen.Diodes.route,
                selectedIconRoute = Screen.Categories.route
            )

        }

    ) {innerPadding ->
//        Row(
//            modifier = Modifier
//                .fillMaxWidth()
//                .background(color = MaterialTheme.colors.primary),
//            horizontalArrangement = Arrangement.End,
//        ) {
//            IconButton(onClick = { showFiltersOnPress() }) {
//                Icon(imageVector = Icons.Rounded.List, contentDescription = "")
//            }
//        }

            Column {
                Spacer(modifier = Modifier.size(8.dp))
                LazyColumn(
                    modifier = Modifier
//                    .background()
                        .fillMaxSize()
                        .padding(innerPadding),
                ) {

                    items(items = uiState.diodeList, key = { it.id }) { diode ->
                        DiodeCard(
                            modifier = Modifier.padding(vertical = 8.dp, horizontal = 24.dp),
                            diode = diode
                        )
                    }
                }
                
            }
    }
}

@Composable
fun DiodeCard(
    modifier: Modifier = Modifier,
    diode: Diode
) {
    Card(
        modifier = modifier
            .fillMaxWidth(),
        shape = RoundedCornerShape(10)
    ) {
        Column(Modifier.padding(horizontal = 16.dp, vertical = 4.dp)) {
            Text(text = diode.name, style = MaterialTheme.typography.h1, )

            Divider(
                color = MaterialTheme.colors.primary,
                thickness = 1.5.dp,
                modifier = Modifier
                    .padding(top = 8.dp)
                    .clip(RoundedCornerShape(50))
            )

            Row (modifier = Modifier.padding(vertical = 16.dp), verticalAlignment = Alignment.Bottom){

                Column(
                    modifier = Modifier.padding(end = 16.dp),
                    verticalArrangement = Arrangement.SpaceAround
                ) {
                    Text(text = "Id :", style = MaterialTheme.typography.body1)
                    Text(text = "Vrwm, B :", style = MaterialTheme.typography.body1)
                    Text(text = "Tol, % :", style = MaterialTheme.typography.body1)
                    Text(text = "Корпус :", style = MaterialTheme.typography.body1)
                }


                Column(
                    modifier = Modifier.weight(2f),
                    verticalArrangement = Arrangement.SpaceAround,

                ) {
                    Text(text = "${diode.id}", style = MaterialTheme.typography.body1)
                    Text(text = "${diode.vrwm}", style = MaterialTheme.typography.body1)
                    Text(text = "${diode.tol}", style = MaterialTheme.typography.body1)
                    Text(text = diode.housing, style = MaterialTheme.typography.body1)
                }
                Column {
                    IconButton(onClick = { }) {
                        Icon(
                            imageVector = ImageVector.vectorResource(R.drawable.baseline_shopping_cart_24),
                            contentDescription = "",
                            tint = MaterialTheme.colors.primary
                        )
                    }
                }
            }
        }
    }
}

@Composable
fun DiodeTopBar(
    title: String,
    canNavigateBack: Boolean,
    navigateBack: () -> Unit = {},
    showFilters: () -> Unit,
) {
    TopAppBar(

        backgroundColor = MaterialTheme.colors.primary,
//        elevation = 0.dp,
    ) {
        Row(modifier = Modifier.fillMaxWidth(), verticalAlignment = Alignment.CenterVertically) {
            if (canNavigateBack){
                IconButton(onClick = { navigateBack() }) {
                    Icon(
                        imageVector = Icons.Default.ArrowBack,
                        contentDescription = "",
                        tint = Color.White
                    )
                }
            }

            Text(text = title, style = MaterialTheme.typography.body1.copy(
                fontSize = 22.sp,
                fontWeight = FontWeight.Bold,
                color = Color.White
            ))

            Spacer(modifier = Modifier.weight(1f))

            IconButton(onClick = { /*TODO*/ }) {
                Icon(
                    modifier = Modifier.size(26.dp),
                    imageVector = ImageVector.vectorResource(R.drawable.quiestion_message),
                    contentDescription = "",
                    tint = Color.White
                )
            }
            IconButton(onClick = showFilters) {
                Icon(
                    modifier = Modifier.size(36.dp),
                    imageVector = ImageVector.vectorResource(R.drawable.baseline_filter_list_24),
                    contentDescription = "",
                    tint = Color.White
                )
            }

        }
    }
}

@Preview
@Composable
fun DiodeCardPreview() {
    LearnRoomTheme {
        Column {
            DiodeCard(
                diode = Diode(
                    id = 0,
                    name = "Cool diode",
                    vrwm = 10,
                    tol = 15,
                    housing = "Cool housing"
                ),
            )

            DiodeCard(
                diode = Diode(
                    id = 0,
                    name = "Cool diode",
                    vrwm = 10,
                    tol = 15,
                    housing = "Cool housing"
                ),
            )
        }

    }
}


//@Preview(showBackground = true)
//@Composable
//fun DiodeCardPreview() {
//    Diodes(
//        uiState = DiodeUiState(
//            listOf(
//                Diode(
//                    id = 0,
//                    name = "Cool diode",
//                    vrwm = 10,
//                    tol = 15,
//                    housing = "Cool housing"
//                ),
//                Diode(
//                    id = 0,
//                    name = "Stupid diode",
//                    vrwm = 1,
//                    tol = 2,
//                    housing = "Shit housing"
//                ),
//                Diode(
//                    id = 0,
//                    name = "Ne diode",
//                    vrwm = 69,
//                    tol = 2048,
//                    housing = "yo"
//                )
//            )
//        ),
//        showFiltersOnPress = {}
//    )
//}
