package com.example.testappforvuz.ui.screens.categories

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.inventory.ui.theme.LearnRoomTheme
import com.example.learnroom.ui.navigation.Screen
import com.example.testappforvuz.R
import com.example.testappforvuz.data.category.Category
import com.example.testappforvuz.ui.MyBottomBar
import com.example.testappforvuz.ui.MyTopBar


@Composable
fun CategoriesScreen(
    modifier: Modifier = Modifier,
    uiState: CategoriesUiState,
    canNavigateBack: Boolean,
    navigateBack: () -> Unit,
    bottomNavigate: (String) -> Unit,
    navigateToCategory: (String) -> Unit
) {

    Scaffold(topBar = {
        MyTopBar(
            title = stringResource(id = Screen.Categories.title),
            canNavigateBack = canNavigateBack,
            navigateBack = navigateBack
        )
    },

        bottomBar = {
            MyBottomBar(
                navigateToNewScreen = { newScreenRoute -> bottomNavigate(newScreenRoute) },
                currentRoute = Screen.Categories.route,
                selectedIconRoute = Screen.Categories.route
            )
        }

    ) { innerPadding ->
        
        Column {
            Spacer(modifier = Modifier.size(8.dp))
            LazyColumn(
                modifier = modifier
                    .fillMaxSize()
                    .padding(innerPadding)
            ) {

                items(items = uiState.categories, key = { it.id }) { category ->

                    CategoryCard(
                        category = category, navigateToCategory = navigateToCategory
                    )
                }
            }
            
        }

    }
}

@Composable
fun CategoryCard(
    category: Category, navigateToCategory: (String) -> Unit
) {

    Card(
        modifier = Modifier
            .height(90.dp)
            .fillMaxWidth()
            .padding(vertical = 8.dp, horizontal = 32.dp)
            .clickable { navigateToCategory(category.path) },
        elevation = 0.dp,
        shape = RoundedCornerShape(16.dp)
    ) {
        Row(
            modifier = Modifier.fillMaxSize(), verticalAlignment = Alignment.CenterVertically
        ) {
            Spacer(modifier = Modifier.weight(1f))
            Text(
                text = stringResource(id = category.title),
                style = MaterialTheme.typography.h1,
                modifier = Modifier.weight(3f)
            )
            Icon(
                imageVector = ImageVector.vectorResource(R.drawable.arrow_circle_up),
                tint = MaterialTheme.colors.primary,
                contentDescription = "",
                modifier = Modifier.weight(1f),
            )
        }
    }
}

@Preview
@Composable
fun CategoriesScreenPreview() {
    LearnRoomTheme {
        CategoryCard(category = Category(0, Screen.Diodes.title, Screen.Diodes.route)) {}
    }
}