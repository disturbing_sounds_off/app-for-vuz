package com.example.testappforvuz.ui.screens.categories.diodes

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.example.learnroom.ui.navigation.Screen
import com.example.testappforvuz.ui.MyBottomBar
import com.example.testappforvuz.ui.MyTopBar


@Composable
fun DiodeFilters(
    tolFilter: String,
    tolOnValueChange: (String) -> Unit,
    onBackButtonPressed: () -> Unit,
    applyFiltersAndGoBack: () -> Unit,
    bottomNavigate: (String) -> Unit
) {

    BackHandler {
        onBackButtonPressed()
    }

    Scaffold(
        topBar = {
            MyTopBar(
                title = stringResource(id = Screen.Diodes.title),
                canNavigateBack = true,
                navigateBack = onBackButtonPressed,
            )
        },

        bottomBar = {
            MyBottomBar(
                navigateToNewScreen = { newScreenRoute -> bottomNavigate(newScreenRoute) },
                currentRoute = Screen.Diodes.route,
                selectedIconRoute = Screen.Categories.route
            )

        }

    ) { innerPadding ->

        Column(Modifier.padding(innerPadding)) {
            Column(
                modifier = Modifier
                    .weight(1f)
            ) {

            }

            Card(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(60.dp)
            ) {
                Button(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 30.dp, vertical = 10.dp),
                    shape = RoundedCornerShape(40),
                    onClick = applyFiltersAndGoBack
                ) {
                    Text(text = "Применить", style = MaterialTheme.typography.h1)
                }
            }

        }

    }
}
