package com.example.testappforvuz.ui.screens.shoppingcart

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import com.example.learnroom.ui.navigation.Screen
import com.example.testappforvuz.ui.MyBottomBar
import com.example.testappforvuz.ui.MyTopBar

@Composable
fun ShoppingCartScreen(
    canNavigateBack: Boolean,
    navigateBack: () -> Unit,
    bottomNavigate: (String) -> Unit
) {
    Scaffold(
        topBar = {
            MyTopBar(
                title = stringResource(id = Screen.ShoppingCart.title ),
                canNavigateBack = canNavigateBack,
                navigateBack = navigateBack
            )
        },

        bottomBar = {
            MyBottomBar(
                navigateToNewScreen = {newScreenRoute -> bottomNavigate(newScreenRoute)},
                currentRoute = Screen.ShoppingCart.route,
                selectedIconRoute = Screen.ShoppingCart.route
            )

        }

    ) {innerPadding ->
        Column(modifier = Modifier.padding(innerPadding)) {

        }
    }

}
