package com.example.testappforvuz.ui.screens.home

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.Button
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.example.testappforvuz.ui.AppViewModelProvider
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.learnroom.ui.navigation.Screen
import com.example.testappforvuz.ui.MyBottomBar
import com.example.testappforvuz.ui.MyTopBar


@Composable
fun HomeScreen(
    modifier: Modifier = Modifier,
    viewModel: HomeViewModel = viewModel(factory = AppViewModelProvider.Factory),
    bottomNavigate: (String) -> Unit
) {
    val homeUiState = viewModel.homeUiState.collectAsState()


    Scaffold(
        topBar = {
            MyTopBar(
                title = stringResource(id = Screen.Home.title ),
                canNavigateBack = false,
                navigateBack = {}
            )
        },

        bottomBar = {
            MyBottomBar(
                navigateToNewScreen = {newScreenRoute -> bottomNavigate(newScreenRoute)},
                currentRoute = Screen.Home.route,
                selectedIconRoute = Screen.Home.route
            )

        }

    ) {innerPadding ->
    Column(
        modifier = modifier.fillMaxSize().padding(innerPadding),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {

        Text(text = "THIS IS HOME SCREEN")
        Spacer(modifier = Modifier.size(16.dp))
        Row(modifier = Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.SpaceAround) {

            Button(onClick = { viewModel.addFakeData() }) {
                Text(text = "Add fake data")
            }

            Button(onClick = { viewModel.clearTable() }) {
                Text(text = "Clear table")
            }

        }

    }
}
}
