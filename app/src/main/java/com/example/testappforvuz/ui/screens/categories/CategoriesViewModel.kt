package com.example.testappforvuz.ui.screens.categories

import androidx.lifecycle.ViewModel
import com.example.learnroom.ui.navigation.Screen
import com.example.testappforvuz.data.AppRemoteRepository
import com.example.testappforvuz.data.category.Category
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow

class CategoriesViewModel(private val remoteRepository: AppRemoteRepository) : ViewModel() {

    val categories: StateFlow<CategoriesUiState> =
        MutableStateFlow(CategoriesUiState()).asStateFlow()

}

data class CategoriesUiState(
    val categories: List<Category> = listOf(
        Category(id = 1, title = Screen.Diodes.title, path = Screen.Diodes.route),
        Category(id = 2, title = Screen.Diodes.title, path = Screen.Diodes.route),
        Category(id = 3, title = Screen.Diodes.title, path = Screen.Diodes.route),
        Category(id = 4, title = Screen.Diodes.title, path = Screen.Diodes.route),
        Category(id = 5, title = Screen.Diodes.title, path = Screen.Diodes.route),
        Category(id = 6, title = Screen.Diodes.title, path = Screen.Diodes.route),
    )
)