package com.example.testappforvuz.ui

import androidx.lifecycle.ViewModelProvider.AndroidViewModelFactory
import androidx.lifecycle.viewmodel.CreationExtras
import androidx.lifecycle.viewmodel.initializer
import androidx.lifecycle.viewmodel.viewModelFactory
import com.example.testappforvuz.VuzApplication
import com.example.testappforvuz.ui.screens.categories.CategoriesViewModel
import com.example.testappforvuz.ui.screens.categories.diodes.DiodesViewModel
import com.example.testappforvuz.ui.screens.home.HomeViewModel

object AppViewModelProvider {
    val Factory = viewModelFactory {

        initializer {
            HomeViewModel(vuzApplication().container.diodeRepository, vuzApplication().container.remoteRepository)
        }

        initializer {
            CategoriesViewModel(vuzApplication().container.remoteRepository)
        }

        initializer {
            DiodesViewModel(vuzApplication().container.diodeRepository, vuzApplication().container.remoteRepository)
        }

    }
}

fun CreationExtras.vuzApplication(): VuzApplication =
    (this[AndroidViewModelFactory.APPLICATION_KEY] as VuzApplication)
