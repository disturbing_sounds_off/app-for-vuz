package com.example.testappforvuz.ui.screens.home

import androidx.compose.runtime.collectAsState
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.testappforvuz.data.AppRemoteRepository
import com.example.testappforvuz.data.DefaultAppRemoteRepository
import com.example.testappforvuz.data.diode.Diode
import com.example.testappforvuz.data.diode.DiodeRepository
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.forEach
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch

class HomeViewModel(
    private val diodeRepository: DiodeRepository,
    private val remoteRepository: AppRemoteRepository
) : ViewModel(){

    val homeUiState = diodeRepository.getAllDiodes()
        .map { HomeUiState(it) }
        .stateIn(
            viewModelScope,
            SharingStarted.WhileSubscribed(5_000L),
            HomeUiState()
        )

    fun addFakeData() {
        viewModelScope.launch {

            diodeRepository.insertDiode(Diode(
                id = 0,
                name = "Cool diode",
                vrwm = 10,
                tol = 15,
                housing = "Cool housing"
            ))

            diodeRepository.insertDiode(Diode(
                id = 0,
                name = "Stupid diode",
                vrwm = 1,
                tol = 2,
                housing = "Shit housing"
            ))

            diodeRepository.insertDiode(Diode(
                id = 0,
                name = "Ne diode",
                vrwm = 69,
                tol = 2048,
                housing = "yo"
            ))

        }
    }

    fun clearTable() {
        viewModelScope.launch {
            val diodes = homeUiState.value.diodeList
            for (i in diodes) {
                diodeRepository.deleteDiode(i)
            }
        }
    }

}
data class HomeUiState(val diodeList: List<Diode> = emptyList())
