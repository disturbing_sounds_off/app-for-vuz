package com.example.learnroom.ui.navigation

import androidx.annotation.StringRes
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.example.testappforvuz.R
import com.example.testappforvuz.ui.AppViewModelProvider
import com.example.testappforvuz.ui.screens.categories.CategoriesScreen
import com.example.testappforvuz.ui.screens.categories.CategoriesUiState
import com.example.testappforvuz.ui.screens.categories.CategoriesViewModel
import com.example.testappforvuz.ui.screens.categories.diodes.DiodeUiState
import com.example.testappforvuz.ui.screens.categories.diodes.DiodesScreen
import com.example.testappforvuz.ui.screens.categories.diodes.DiodesViewModel
import com.example.testappforvuz.ui.screens.home.HomeScreen
import com.example.testappforvuz.ui.screens.producers.ProducersScreen
import com.example.testappforvuz.ui.screens.shoppingcart.ShoppingCartScreen

interface WithIcon {
    val icon: Int
}

sealed class Screen(val route: String, @StringRes val title: Int){
    object Home: Screen("home", R.string.app_name), WithIcon { override val icon = R.drawable.baseline_home_24}
    object Categories: Screen("categories", R.string.categories), WithIcon { override val icon = R.drawable.baseline_memory_24}
    object Producers: Screen("producers", R.string.producers), WithIcon { override val icon = R.drawable.baseline_engineering_24}
    object ShoppingCart: Screen("shopping_cart", R.string.cart), WithIcon { override val icon = R.drawable.baseline_shopping_cart_24}
    object Diodes: Screen("diodes", R.string.diodes)
}

//enum class Scrn(@StringRes val title: Int, val icon: Int) {
//    Home(title= R.string.app_name, icon = R.drawable.baseline_home_24) {val test: String = ""},
//    Categories(title= R.string.categories, icon = R.drawable.baseline_memory_24),
//    Producers(title= R.string.producers, icon = R.drawable.baseline_engineering_24),
//    ShoppingCart(title= R.string.cart, icon = R.drawable.baseline_shopping_cart_24),
//}



@Composable
fun MyNavHost(
    navController: NavHostController,
    modifier: Modifier = Modifier
) {


    NavHost(navController = navController, startDestination = Screen.Home.route, modifier = modifier ) {

        composable(route = Screen.Home.route) {
            HomeScreen(bottomNavigate = { newRoute -> navController.navigate(newRoute) })
        }

        composable(route = Screen.Categories.route) {
            val viewModel: CategoriesViewModel = viewModel(factory = AppViewModelProvider.Factory)
            val uiState: CategoriesUiState by viewModel.categories.collectAsState()
            CategoriesScreen(
                uiState = uiState,
                canNavigateBack = navController.currentBackStackEntry != null,
                navigateBack = {navController.popBackStack(route = Screen.Home.route, inclusive = false)},
                bottomNavigate = {newRoute -> navController.navigate(newRoute)},
                navigateToCategory = {newRoute -> navController.navigate(newRoute)},
            )
        }

        composable(route = Screen.Producers.route) {
            ProducersScreen(
                canNavigateBack = navController.currentBackStackEntry != null,
                navigateBack = {navController.popBackStack(route = Screen.Home.route, inclusive = false)},
                bottomNavigate = {newRoute -> navController.navigate(newRoute)}
            )
        }

        composable(route = Screen.ShoppingCart.route) {
            ShoppingCartScreen(
                canNavigateBack = navController.currentBackStackEntry != null,
                navigateBack = {navController.popBackStack(route = Screen.Home.route, inclusive = false)},
                bottomNavigate = {newRoute -> navController.navigate(newRoute)}
            )
        }

        composable(route = Screen.Diodes.route) {

            val viewModel: DiodesViewModel = viewModel(factory = AppViewModelProvider.Factory)
            val uiState: DiodeUiState by viewModel.diodeUiState.collectAsState()

            DiodesScreen(
                uiState = uiState,
                canNavigateBack = navController.currentBackStackEntry != null,
                navigateBack = {navController.navigateUp()},
                bottomNavigate = {newRoute -> navController.navigate(newRoute)}
            )
        }

    }
}
