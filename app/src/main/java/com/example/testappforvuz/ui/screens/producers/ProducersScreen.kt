package com.example.testappforvuz.ui.screens.producers

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Card
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import com.example.learnroom.ui.navigation.Screen
import com.example.testappforvuz.ui.MyBottomBar
import com.example.testappforvuz.ui.MyTopBar

@Composable
fun ProducersScreen(
    canNavigateBack: Boolean,
    navigateBack: () -> Unit,
    bottomNavigate: (String) -> Unit
) {
    Scaffold(
        topBar = {
            MyTopBar(
                title = stringResource(id = Screen.Producers.title ),
                canNavigateBack = canNavigateBack,
                navigateBack = navigateBack
            )
        },

        bottomBar = {
            MyBottomBar(
                navigateToNewScreen = {newScreenRoute -> bottomNavigate(newScreenRoute)},
                currentRoute = Screen.Producers.route,
                selectedIconRoute = Screen.Producers.route
            )

        }

    ) { innerPadding ->
        Column(modifier = Modifier.padding(innerPadding)) {

        }
    }

}