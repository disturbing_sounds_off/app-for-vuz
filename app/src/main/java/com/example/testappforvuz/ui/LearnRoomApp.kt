package com.example.testappforvuz.ui

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.example.inventory.ui.theme.LearnRoomTheme
import com.example.learnroom.ui.navigation.MyNavHost
import com.example.learnroom.ui.navigation.Screen


@Composable
fun LearnRoomApp(
    navController: NavHostController = rememberNavController()
) {

    MyNavHost(navController = navController, modifier = Modifier.padding())
}


@Composable
fun MyTopBar(
    title: String,
    canNavigateBack: Boolean,
    navigateBack: () -> Unit = {},
) {
    TopAppBar(

        backgroundColor = MaterialTheme.colors.primary,
//        elevation = 1132.dp,
    ) {
        Row(modifier = Modifier.fillMaxWidth(), verticalAlignment = Alignment.CenterVertically) {
            if (canNavigateBack){
                IconButton(onClick = { navigateBack() }) {
                    Icon(
                        imageVector = Icons.Default.ArrowBack,
                        contentDescription = "",
                        tint = Color.White
                    )
                }
            }

            Text(text = title, style = MaterialTheme.typography.body1.copy(
                fontSize = 22.sp,
                fontWeight = FontWeight.Bold,
                color = Color.White
            ))
        }
    }
}

@Composable
fun MyBottomBar(
    navigateToNewScreen: (String) -> Unit,
    selectedIconRoute: String,
    currentRoute: String
) {
    val bottomBarElements = listOf(
        Screen.Home,
        Screen.Categories,
        Screen.Producers,
        Screen.ShoppingCart
    )
    Row (
        modifier = Modifier
            .fillMaxWidth()
            .background(Color(0xFFFF8517)),
        horizontalArrangement = Arrangement.SpaceEvenly
    ){



        for (it in bottomBarElements) {
            IconButton(
                modifier = Modifier
                    .padding(5.dp)
                    .then(
                        if (it.route == selectedIconRoute) {
                            Modifier
                                .clip(RoundedCornerShape(50))
                                .background(color = Color.White)
                        } else {
                            Modifier
                        }

                    ),
                onClick = {
                    if ( it.route != currentRoute) {
                        navigateToNewScreen(it.route)
                    }
                }
            ) {
                Icon(
                    imageVector = ImageVector.vectorResource(it.icon),
                    contentDescription = stringResource(id = it.title),
                    tint = if (it.route == selectedIconRoute) MaterialTheme.colors.primary else Color.White
                )
            }
        }
    }

//    BottomNavigation(elevation = 5.dp) {
//        bottomBarElements.map {
//            BottomNavigationItem (
//                modifier = Modifier.then(
//                    if ( currentRoute == it.name) {
//                        Modifier
//                            .background(color = Color.White,)
//                            .clip(shape = RoundedCornerShape(50))
//                    } else { Modifier}
//                ),
//                selected = currentRoute == it.name,
//                onClick = { navigateToNewScreen(it.name) },
//                icon = {
//                    Icon(imageVector = it.icon , contentDescription = stringResource(id = it.title) )
//                },
//                selectedContentColor= Color.Red,
//                unselectedContentColor= Color.White.copy(alpha = 0.4f),
//
//            )
//        }
//
//    }
}

@Preview
@Composable
fun TitleBarPreview() {
    LearnRoomTheme {
        MyTopBar(title = "Каталог", canNavigateBack = true)
    }
}

@Preview
@Composable
fun BottomNavigationPreview() {
    LearnRoomTheme {
        MyBottomBar(
            navigateToNewScreen = {},
            currentRoute = Screen.ShoppingCart.route,
            selectedIconRoute = Screen.ShoppingCart.route
        )
    }
}

