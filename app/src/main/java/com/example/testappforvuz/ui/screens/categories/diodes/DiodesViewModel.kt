package com.example.testappforvuz.ui.screens.categories.diodes

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.testappforvuz.data.AppRemoteRepository
import com.example.testappforvuz.data.diode.Diode
import com.example.testappforvuz.data.diode.DiodeRepository
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch

class DiodesViewModel(
    diodeRepository: DiodeRepository,
    remoteRepository: AppRemoteRepository
): ViewModel() {

    val diodeUiState: StateFlow<DiodeUiState> = diodeRepository.getAllDiodes()
        .map { DiodeUiState(it) }
        .stateIn(
            scope = viewModelScope,
            started = SharingStarted.WhileSubscribed(stopTimeoutMillis = TIMEOUT_MILLIS),
            initialValue = DiodeUiState()
        )

    companion object {
        private const val TIMEOUT_MILLIS = 5_000L
    }

}

data class DiodeUiState(val diodeList: List<Diode> = emptyList())